/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

let fs = require('fs');



function fsCallbackProblem2(file){
    fs.readFile(file, 'utf8', function(err, data){
        if (err) console.log(err);
        else{
            console.log("lipsum.txt is read")
            fs.writeFile('test/test2/toUpperCase.txt', data.toUpperCase(), err =>{
                if(err) console.log(err);
                else{
                    console.log("toUpperCase.txt is written.");
                    fs.writeFile('test/test2/fileNames.txt', "toUpperCase.txt\n", err =>{
                        if(err) console.error(err);
                        else{
                            console.log("fileNames.txt updated");
                            fs.readFile("test/test2/toUpperCase.txt", 'utf8',(err,data) =>{
                                if(err) console.log(err);
                                else{
                                    console.log("toUpperCase.txt is read")
                                    fs.writeFile("test/test2/lowerCaseAndSplit.txt", data.toLowerCase().replaceAll(".",".\n"), err =>{
                                        if(err) console.log(err);
                                        else{
                                            console.log("toLowerCaseAndSplit.txt is written.");
                                            fs.appendFile("test/test2/fileNames.txt", "lowerCaseAndSplit.txt\n",err =>{
                                                if(err) console.log(err);
                                                else{
                                                    console.log("fileNames.txt updated");
                                                    fs.readFile("test/test2/lowerCaseAndSplit.txt", 'utf-8', (err, data) =>{
                                                        if(err) console.log(err);
                                                        else{
                                                            console.log("lowerCaseAndSplit.txt is read");
                                                            fs.writeFile("test/test2/sorted.txt", data.split("\n").sort().join("\n"), err => {
                                                                if(err) console.log(err);
                                                                else{
                                                                    console.log("sorted.txt is written")
                                                                    fs.appendFile("test/test2/fileNames.txt", "sorted.txt\n", err=>{
                                                                        if(err) console.log(err)
                                                                        else{
                                                                            console.log("fileNames.txt updated");
                                                                            fs.readFile("test/test2/fileNames.txt", "utf-8", (err,data) =>{
                                                                                if(err) console.log(err);
                                                                                else{
                                                                                    let files = data.split("\n");
                                                                                    fs.unlink("test/test2/"+files[0],err=>{
                                                                                        if(err) console.log(err);
                                                                                        else{
                                                                                            console.log("toUpperCase.txt is deleted");
                                                                                        }
                                                                                    })
                                                                                    fs.unlink("test/test2/"+files[1],err=>{
                                                                                        if(err) console.log(err);
                                                                                        else{
                                                                                            console.log("toLowerCaseAndSplit.txt is deleted");
                                                                                        }
                                                                                    })
                                                                                    fs.unlink("test/test2/"+files[2],err=>{
                                                                                        if(err) console.log(err);
                                                                                        else{
                                                                                            console.log("sorted.txt deleted");
                                                                                        }
                                                                                    })
                                                                                    fs.unlink("test/test2/fileNames.txt",err =>{
                                                                                        if(err) console.log(err);
                                                                                        else{
                                                                                            console.log("fileNames.txt is deleted");
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            });
        }
    });
}

module.exports = fsCallbackProblem2;


