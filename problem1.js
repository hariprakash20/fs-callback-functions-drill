/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
let fs = require('fs');

function createAndDeleteRandomJsonFiles(folderName , numberOfFiles){
    fs.mkdir(folderName, (err) => {
        if (err)
            return console.error(err);
        else{
            console.log('Directory created successfully!');
            for(let i=0;i<numberOfFiles;i++){
                let data = JSON.stringify(require('./random.json'));
                fs.writeFile("test/test1/random"+(i+1)+".json", data, (err) => {
                if (err)
                  console.log(err);
                else {
                  console.log("File written successfully");
                  fs.unlink("test/test1/random"+(i+1)+".json", (err => {
                    if (err) console.log(err);
                    else {
                      console.log("Deleted file");
                    }
                  }));
                }
            });
            }
        }   
    });
}

module.exports =createAndDeleteRandomJsonFiles;

